#include<stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <validacion.h>
#include <vector.h>
int main() {
/*Encabezado para leer de librerías dinámicas */

void *handle;

char* (*prueba)();
vector3D* (*newVector)(float , float , float );
float (*dotproduct)(vector3D, vector3D);
float (*magnitud)(vector3D);
int (*esOrtogonal)(vector3D, vector3D);
vector3D (*crossproduct)(vector3D, vector3D);

char *error;
/* Dynamically load shared library that contains addvec() */
handle = dlopen("./libvector.so", RTLD_LAZY);
if (!handle) {
	fprintf(stderr, "%s\n", dlerror());
	exit(1);
}

 /* Arroja un puntero a prueba() que fue cargada arriba */

prueba = dlsym(handle, "prueba");
newVector= dlsym(handle, "newVector");
dotproduct = dlsym(handle, "dotproduct");
magnitud = dlsym(handle, "magnitud");
esOrtogonal= dlsym(handle, "esOrtogonal");
crossproduct= dlsym(handle, "crossproduct");


/*COMIENZA CÓDIGO DEL PROGRAMA*/
 
	float x1,y1,z1,x2,y2,z2;
	char xc[15],yc[15],zc[15];
	int formato;
	vector3D *vector1,*vector2, prodCruz;
	printf("Práctica creación de librerías en C\nPrograma de operaciones en vectores\n\n");
	do
	{
		printf("\nIngrese el primer vector: (x y z)");
		scanf("%s %s %s",xc,yc,zc);
		formato=format(xc,yc,zc);
	}
	while(formato==0);
	x1=atof(xc);
	y1=atof(yc);
	z1=atof(zc);
	vector1=newVector(x1,y1,z1);
	do
	{
		printf("\nIngrese el segundo vector (x y z): ");
		scanf("%s %s %s",xc,yc,zc);
                formato=format(xc,yc,zc);
	}
	while(formato==0);
	x2=atof(xc);
	y2=atof(yc);
	z2=atof(zc);
	vector2=newVector(x2,y2,z2);
	printf("\nMagnitud del primer vector: %.4f",magnitud(*vector1));
	printf("\nMagnitud del segundo vector: %.4f",magnitud(*vector2));
	prodCruz=crossproduct(*vector1,*vector2);
	printf("\nEl producto vectorial entre los vectores es: %.4f %.4f %.4f",prodCruz.x,prodCruz.y,prodCruz.z);
	printf("\nEl producto escalar entre los vectores es: %.4f",dotproduct(*vector1,*vector2));
	if(esOrtogonal(*vector1,*vector2)==1)
	{
		printf("\nLos vectores son ortogonales");
	}
	else
	{
		printf("\nLos vectores no son ortogonales");
	}

/*FINALIZA CÓDIGO DEL PROGRAMA*/


/* Unload the shared library */
if (dlclose(handle) < 0) {
fprintf(stderr, "%s\n", dlerror());
exit(1);
}
return 0;


}
