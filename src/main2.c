#include<stdio.h>
#include <stdlib.h>
#include "vector.h"
#include <validacion.h>
int main(int argc, char const *argv[])
{
	float x1,y1,z1,x2,y2,z2;
	char xc[15],yc[15],zc[15];
	int formato;
	vector3D *vector1,*vector2, prodCruz;
	printf("Práctica creación de librerías en C\nPrograma de operaciones en vectores\n\n");
	do
	{
		printf("\nIngrese el primer vector: (x y z)");
		scanf("%s %s %s",xc,yc,zc);
		formato=format(xc,yc,zc);
	}
	while(formato==0);
	x1=atof(xc);
	y1=atof(yc);
	z1=atof(zc);
	vector1=newVector(x1,y1,z1);
	do
	{
		printf("\nIngrese el segundo vector (x y z): ");
		scanf("%s %s %s",xc,yc,zc);
                formato=format(xc,yc,zc);
	}
	while(formato==0);
	x2=atof(xc);
	y2=atof(yc);
	z2=atof(zc);
	vector2=newVector(x2,y2,z2);
	printf("\nMagnitud del primer vector: %.4f",magnitud(*vector1));
	printf("\nMagnitud del segundo vector: %.4f",magnitud(*vector2));
	prodCruz=crossproduct(*vector1,*vector2);
	printf("\nEl producto vectorial entre los vectores es: %.4f %.4f %.4f",prodCruz.x,prodCruz.y,prodCruz.z);
	printf("\nEl producto escalar entre los vectores es: %.4f",dotproduct(*vector1,*vector2));
	if(esOrtogonal(*vector1,*vector2)==1)
	{
		printf("\nLos vectores son ortogonales");
	}
	else
	{
		printf("\nLos vectores no son ortogonales");
	}

}
