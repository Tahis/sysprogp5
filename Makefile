# Este es el archivo Makefile de la práctica, editarlo como sea neceario
CC=gcc
CFLAGS=-Wall -ggdb -fPIC
LDFLAGS=-fPIC -shared

# Target que se ejecutará por defecto con el comando make
all: dynamic

# Target que compilará el proyecto usando librerías estáticas
static: libvector.a main2.o validacion.o
	gcc -o static main2.o ./libvector.a validacion.o -static -lm 


libvector.a: vector.o
	ar rcs libvector.a vector.o

main2.o: src/main2.c include/vector.h include/validacion.h
	gcc -c src/main2.c -I./include  include/validacion.h include/vector.h   


# Target que compilará el proyecto usando librerías dinámicas
dynamic: libvector.so src/main.c validacion.o
	$(CC) -rdynamic -o dynamic  src/main.c validacion.o -I./include include/vector.h -ldl -lm 

libvector.so: vector.o
	$(CC) $(LDFLAGS) -o $@ $^ -lm
	
vector.o: lib/vector.c include/vector.h 
	$(CC) -fPIC -c lib/vector.c -I./include include/vector.h  
validacion.o: lib/validacion.c include/validacion.h
	$(CC) -fPIC -c lib/validacion.c -I./include include/validacion.h
# Limpiar archivos temporales
clean:
	rm *.o dynamic static