#include<vector.h>
#include<math.h>
#include <stdlib.h>
#include<string.h>
/* implementar aquí las funciones requeridas */
float dotproduct(vector3D v1, vector3D v2){
	return v1.x*v2.x+v1.y*v2.y+v1.z*v2.z;
}
char* prueba(char * palabra){
	
	return palabra;
}
vector3D crossproduct(vector3D u, vector3D v){
	vector3D resultante;
	float xr = u.y*v.z-u.z*v.y;
	float yr= u.x*v.z -v.x*u.z;
	float zr = u.x*v.y-v.x*u.y;
	resultante.x = xr;
	resultante.y = yr;
	resultante.z = zr;
	return resultante;

}

vector3D *newVector(float x, float y, float z){
  vector3D *v;

  if((v = malloc(sizeof *v)) != NULL)
  {
    v->x = x;
    v->y = y;
    v->z = z;
  }
  return v;
}

float magnitud(vector3D vector){
	return sqrt(dotproduct(vector,vector));
}

int esOrtogonal(vector3D v1, vector3D v2){
	int punto = dotproduct( v1,  v2);
	if(punto != 0){
		return 0;
	}else{
		return 1;
	}
}